package com.example.thanhhai.helloapp;

import java.util.ArrayList;

public class User{
    String id;
    UserBasicInfo userBasicInfo;
    Images images;
    UserIntro userIntro;
    UserLooking userLooking;


    public User() {
        id = "0";
    }

    public User(String id, UserBasicInfo userBasicInfo, Images images, UserIntro userIntro, UserLooking userLooking) {
        this.id = id;
        this.userBasicInfo = userBasicInfo;
        this.images = images;
        this.userIntro = userIntro;
        this.userLooking = userLooking;
    }
}

